//
//  Student.hpp
//  Loop-Queue-in-Cpp
//
//  Created by 买明 on 21/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#ifndef Student_hpp
#define Student_hpp

#include <stdio.h>
#include <ostream>

class Student {
    // 重载 << 运算符
    friend ostream &operator<<(ostream &out, Student &s);
    
public:
    Student(int no = 0, int age = 0);
    
private:
    int stu_no;
    int stu_age;
};

Student::Student(int no, int age) {
    stu_no = no;
    stu_age = age;
}

ostream &operator<<(ostream &out, Student &s) {
    out << "[no: " << s.stu_no << ", age: " << s.stu_age << "]";
    return out;
}

#endif /* Student_hpp */
