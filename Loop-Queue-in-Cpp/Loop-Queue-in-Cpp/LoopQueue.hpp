//
//  LoopQueue.hpp
//  Loop-Queue-in-Cpp
//
//  Created by 买明 on 21/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#ifndef LoopQueue_hpp
#define LoopQueue_hpp

#include <stdio.h>

using namespace std;

template <typename T>
class LoopQueue {
    
public:
    LoopQueue(int queueCapacity);
    ~LoopQueue();
    void ClearQueue();
    bool QueueEmpty();
    bool QueueFull();
    int QueueLength();
    bool EnQueue(T element);
    bool DeQueue(T &element);
    void QueueTraverse();
    
private:
    T *m_pQueue;
    int m_iQueueLen;
    int m_iQueueCapacity;
    
    int m_iHead;
    int m_iTail;
};

template <typename T>
LoopQueue<T>::LoopQueue(int queueCapacity) {
    m_iQueueCapacity = queueCapacity;
    m_pQueue = new T[m_iQueueCapacity];
    
    ClearQueue();
}

template <typename T>
LoopQueue<T>::~LoopQueue() {
    delete [] m_pQueue;
    m_pQueue = NULL;
}

template <typename T>
void LoopQueue<T>::ClearQueue() {
    m_iHead = 0;
    m_iTail = 0;
    m_iQueueLen = 0;
}

template <typename T>
bool LoopQueue<T>::QueueEmpty() {
    return m_iQueueLen == 0;
}

template <typename T>
bool LoopQueue<T>::QueueFull() {
    return m_iQueueLen == m_iQueueCapacity;
}

template <typename T>
int LoopQueue<T>::QueueLength() {
    return m_iQueueLen;
}

template <typename T>
bool LoopQueue<T>::EnQueue(T element) {
    if (QueueFull()) {
        return false;
    }
    
    m_pQueue[m_iTail] = element;
    m_iTail = ++ m_iTail % m_iQueueCapacity;
    m_iQueueLen ++;
    return true;
}

template <typename T>
bool LoopQueue<T>::DeQueue(T &element) {
    if (QueueEmpty()) {
        return false;
    }
    
    element = m_pQueue[m_iHead];
    m_iHead = ++ m_iHead % m_iQueueCapacity;
    m_iQueueLen --;
    return true;
}

template <typename T>
void LoopQueue<T>::QueueTraverse() {
    for (int i = m_iHead; i < m_iHead + m_iQueueLen; i ++) {
        cout << m_pQueue[i % m_iQueueLen] << " ";
    }
    cout << endl;
}

#endif /* LoopQueue_hpp */
