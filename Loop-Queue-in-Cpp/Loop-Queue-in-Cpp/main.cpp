//
//  main.cpp
//  Loop-Queue-in-Cpp
//
//  Created by 买明 on 21/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#include <iostream>
#include "LoopQueue.hpp"
#include "Student.hpp"

using namespace std;

void testIntLoopQueue();
void testObjectLoopQueue();

int main(int argc, const char * argv[]) {
    
    cout << "testIntLoopQueue" << endl;
    testIntLoopQueue();
    
    cout << "testObjectLoopQueue" << endl;
    testObjectLoopQueue();
    return 0;
}

void testIntLoopQueue() {
    LoopQueue<int> *q = new LoopQueue<int>(5);
    
    q->EnQueue(2);
    q->EnQueue(5);
    
    int e = 0;
    q->DeQueue(e);
    cout << "e: " << e << endl;
    
    q->EnQueue(7);
    q->EnQueue(12);
    q->EnQueue(19);
    q->EnQueue(31);
    q->EnQueue(50);
    
    q->QueueTraverse();
    
    q->ClearQueue();
    
    q->QueueTraverse();
    
    delete q;
    q = NULL;
}

void testObjectLoopQueue() {
    LoopQueue<Student> *q = new LoopQueue<Student>(5);
    
    q->EnQueue(Student(2, 2));
    q->EnQueue(Student(5, 5));
    
    Student e;
    q->DeQueue(e);
    cout << "e: " << e << endl;
    
    q->EnQueue(Student(7, 7));
    q->EnQueue(Student(12, 12));
    q->EnQueue(Student(19, 19));
    q->EnQueue(Student(31, 31));
    q->EnQueue(Student(50, 50));
    
    q->QueueTraverse();
    
    q->ClearQueue();
    
    q->QueueTraverse();
    
    delete q;
    q = NULL;
}
